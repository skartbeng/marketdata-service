package com.karthi.marketdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketdataServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketdataServiceApplication.class, args);
	}

}
