package com.karthi.marketdata.dataprovider;

import java.time.LocalDate;
import java.util.Collection;

public interface MarketDataService {
    Collection<EqPrice> getStocks(LocalDate valueDate, String symbols);
}
