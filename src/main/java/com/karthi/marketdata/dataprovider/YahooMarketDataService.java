package com.karthi.marketdata.dataprovider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Component
@Profile("MarketData")
public class YahooMarketDataService implements MarketDataService {
    @Override
    public Collection<EqPrice> getStocks(LocalDate valueDate, String symbol) {

        try {
            Stock stock = YahooFinance.get(symbol);
            return Stream.of(EqPrice.builder()
                    .currency(stock.getCurrency())
                    .name(stock.getName())
                    .symbol(stock.getSymbol())
                    .build()).collect(Collectors.toList());
        } catch (IOException e) {
            log.error("An exception thrown when reteriveing price for [{}]",symbol,e);
        }

        return Collections.EMPTY_LIST;
    }
}
