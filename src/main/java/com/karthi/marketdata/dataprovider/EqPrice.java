package com.karthi.marketdata.dataprovider;

import lombok.Builder;
import lombok.Value;

import javax.persistence.*;

@Value
@Builder
@Entity
@Table(name = "EQ_PRICE")
public class EqPrice {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "SEQ_EQ_PRICE"
    )
    @SequenceGenerator(
            name = "SEQ_EQ_PRICE",
            allocationSize = 5
    )
    private Long id;
    private String currency;
    private String name;
    private String symbol;
}
