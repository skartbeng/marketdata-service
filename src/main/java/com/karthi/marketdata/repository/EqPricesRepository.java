package com.karthi.marketdata.repository;

import com.karthi.marketdata.dataprovider.EqPrice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EqPricesRepository extends JpaRepository<EqPrice,Long> {
}
