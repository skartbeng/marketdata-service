package com.karthi.marketdata.graphql;

import graphql.schema.idl.TypeRuntimeWiring;

public interface GraphQLTypeSource {
    void appendQuerySources(TypeRuntimeWiring.Builder builder);
    void appendMutatorSources(TypeRuntimeWiring.Builder builder);
}
